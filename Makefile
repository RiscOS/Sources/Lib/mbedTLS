#
# Copyright (c) 2018, RISC OS Open Ltd
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# Makefile for mbedTLS
#

COMPONENT  = mbedTLS
OBJS_X509  = certs pkcs11 x509 \
             x509_create x509_crl x509_crt \
             x509_csr x509write_crt x509write_csr
OBJS_TLS =   debug mps_reader mps_trace ssl_cache \
             ssl_ciphersuites ssl_cli \
             ssl_cookie ssl_msg ssl_srv ssl_ticket \
             ssl_tls ssl_tls13_keys net_sockets
OBJS_CRYPTO = aes aesni aria arc4 \
             asn1parse asn1write base64 \
             bignum blowfish camellia \
             ccm cmac chacha20 chachapoly cipher cipher_wrap \
             constant_time ctr_drbg des dhm \
             ecdh ecdsa ecjpake \
             ecp ecp_curves \
             entropy entropy_poll \
             error gcm havege \
             hkdf hmac_drbg md md2 md4 md5 \
             memory_buffer_alloc nist_kw oid \
             padlock pem pk \
             pk_wrap pkcs12 pkcs5 \
             pkparse pkwrite platform platform_util \
             poly1305 ripemd160 rsa rsa_internal sha1 \
             sha256 sha512 threading timing \
             version \
             version_features xtea
OBJS_PSA   = psa_crypto psa_crypto_aead psa_crypto_cipher psa_crypto_client \
             psa_crypto_driver_wrappers psa_crypto_ecp psa_crypto_hash psa_crypto_mac \
             psa_crypto_rsa psa_crypto_se psa_crypto_slot_management psa_crypto_storage \
             psa_its_file
OBJS       = ${OBJS_X509} ${OBJS_TLS} ${OBJS_CRYPTO} ${OBJS_PSA}
HDRS       = ro_config ro_crypto_config check_config other_mbedtls_hdrs
CINCLUDES  = ${TCPIPINC}
CDEFINES   = -DMBEDTLS_CONFIG_FILE="\"ro_config.h\"" -DRISCOS
CFLAGS     = ${C_NOWARN_NON_ANSI_INCLUDES}
SOURCES_TO_SYMLINK = $(wildcard mbedtls/h/*) $(wildcard psa/h/*)

include CLibrary

ifeq (,${MAKE_VERSION})

# RISC OS / amu case

exphdr.ro_config:
	${AWK} -f h.ro_configawk h.ro_config > ${EXPDIR}.h.ro_config

else

# Posix / gmake case

ro_config.exphdr:
	${AWK} -f ro_configawk.h ro_config.h > ${EXPDIR}/ro_config.h

endif

other_mbedtls_hdrs.exphdr:
	${CP} mbedtls ${EXPDIR}${SEP}mbedtls ${CPFLAGS}
	${CP} psa     ${EXPDIR}${SEP}psa     ${CPFLAGS}

# Dynamic dependencies:
